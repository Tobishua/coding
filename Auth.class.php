<?php
	class Auth {
		public function getSession() {
			if (!empty($_COOKIE['sess']) && !empty($_COOKIE['auth_key']) && empty($_SESSION['uid'])) {
				$db_class = new DB();
				$mysqli = $db_class->connect();
				$old_sess = $mysqli->real_escape_string($_COOKIE['sess']);
				$result = $mysqli->query("SELECT * FROM `users_sessions` WHERE `session`='$old_sess';");
				if ($result->num_rows > 0) {
					$session = $result->fetch_assoc();
					$auth_key = $mysqli->real_escape_string($_COOKIE['auth_key']);
					$uid = $session['uid'];
					$result = $mysqli->query("SELECT COUNT(*) FROM `auth` WHERE `auth_key`='$auth_key' AND `uid`='$uid';");
					if ($result->num_rows > 0) {
						$result = $mysqli->query("SELECT * FROM `users` WHERE `id`='$uid';");
						if ($result->num_rows > 0) {
							$user = $result->fetch_assoc();
							$_SESSION['uid'] = $uid = $user['id'];
							$_SESSION['telegram_id'] = $user['telegram_id'];
							$_SESSION['username'] = $user['username'];
							$time = time();
							$sess = hash('sha256', $user['id'].':'.$user['telegram_id'].':'.$config['salt'].':'.$time);
							setcookie("sess", $sess, time()+604800, "/", "", 1, TRUE);
							require_once "$_SERVER[DOCUMENT_ROOT]/core/inc/classes/IPTool.class.php";
							$ip = new IPTool();
							$real_ip = $ip->get();
							$mysqli->query("INSERT INTO `users_sessions`(`uid`,`ip`,`time`,`session`) VALUES('$uid','$real_ip','$time','$sess');");
							$mysqli->query("DELETE FROM `users_sessions` WHERE `session`='$old_sess';");
						}
					}
					else {
						unset($_SESSION['uid'], $_SESSION['telegram_id'], $_SESSION['username']);
						setcookie("sess", NULL, time()+1, "/", "", 1, TRUE);
						setcookie("auth_key", NULL, time()+1, "/", "", 1, TRUE);
					}
				}
				else {
					unset($_SESSION['uid'], $_SESSION['telegram_id'], $_SESSION['username']);
					setcookie("sess", NULL, time()+1, "/", "", 1, TRUE);
					setcookie("auth_key", NULL, time()+1, "/", "", 1, TRUE);
				}
			}
		}
		private function getKey() {
			if (!empty($_COOKIE['auth_key']) && empty($_SESSION['uid'])) {
				$db_class = new DB();
				$mysqli = $db_class->connect();
				$key = $mysqli->real_escape_string($_COOKIE['auth_key']);
				$result = $mysqli->query("SELECT * FROM `auth` WHERE `auth_key`='$key';");
				if ($result->num_rows > 0) {
					$auth_key = $result->fetch_assoc();
					if ($auth_key['uid'] != 0) {
						$uid = $auth_key['uid'];
						$result_user = $mysqli->query("SELECT * FROM `users` WHERE `id`='$uid';");
						if ($result_user->num_rows > 0) {
							$user = $result_user->fetch_assoc();
							$time = time();
							$mysqli->query("UPDATE `users` SET `last_signin`='$time' WHERE `id`='$uid';");
							$_SESSION['uid'] = $user['id'];
							$_SESSION['telegram_id'] = $user['telegram_id'];
							$_SESSION['username'] = $user['username'];
							$sess = hash('sha256', $user['id'].':'.$user['telegram_id'].':'.$config['salt'].':'.$time);
							setcookie("sess", $sess, time()+604800, "/", "", 1, TRUE);
							require_once "$_SERVER[DOCUMENT_ROOT]/core/inc/classes/IPTool.class.php";
							$ip = new IPTool();
							$real_ip = $ip->get();
							$mysqli->query("INSERT INTO `users_sessions`(`uid`,`ip`,`time`,`session`) VALUES('$uid','$real_ip','$time','$sess');");
							return $_COOKIE['auth_key'];
						}
					}
					return $_COOKIE['auth_key'];
				}
			}
			return NULL;
		}
		public function setKey() {
			if (empty($this->getKey()) && empty($_SESSION['uid'])) {
				$db_class = new DB();
				$mysqli = $db_class->connect();
				$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$charactersLength = strlen($characters);
				$key = '';
				for ($i = 0; $i < 12; $i++) {
					$key .= $characters[rand(0, $charactersLength - 1)];
				}
				setcookie("auth_key", $key, time()+604800, "/", "", 1, TRUE);
				$_SESSION['auth_key'] = $key;
				$time = time();
				$mysqli->query("INSERT INTO `auth`(`auth_key`,`time`) VALUES('$key','$time');");
			}
			else {
				$key = $_SESSION['auth_key'] = $this->getKey();
			}
			return $key;
		}
		public function checkKey($key) {
			$db_class = new DB();
			$mysqli = $db_class->connect();
			$key = $mysqli->real_escape_string($key);
			$result = $mysqli->query("SELECT * FROM `auth` WHERE `auth_key`='$key';");
			if ($result->num_rows > 0) {
				$auth_key = $result->fetch_assoc();
				if ($auth_key['uid'] != 0) {
					$uid = $auth_key['uid'];
					$result_user = $mysqli->query("SELECT * FROM `users` WHERE `id`='$uid';");
					if ($result_user->num_rows > 0) {
						$user = $result_user->fetch_assoc();
						$time = time();
						$mysqli->query("UPDATE `users` SET `last_signin`='$time' WHERE `id`='$uid';");
						$_SESSION['uid'] = $user['id'];
						$_SESSION['telegram_id'] = $user['telegram_id'];
						$_SESSION['username'] = $user['username'];
						$sess = hash('sha256', $user['id'].':'.$user['telegram_id'].':'.$config['salt'].':'.$time);
						setcookie("sess", $sess, time()+604800, "/", "", 1, TRUE);
						require_once "$_SERVER[DOCUMENT_ROOT]/core/inc/classes/IPTool.class.php";
						$ip = new IPTool();
						$real_ip = $ip->get();
						$mysqli->query("INSERT INTO `users_sessions`(`uid`,`ip`,`time`,`session`) VALUES('$uid','$real_ip','$time','$sess');");
						$mysqli->query("DELETE FROM `auth` WHERE `auth_key`='$key';");
						$return = array(
							'state'	=>	'success',
							'sess'	=>	$sess
						);
						return json_encode($return);
					}
				}
				else {
					$return = array(
						'state'	=>	'not_yet'
					);
					return json_encode($return);
				}
			}
			else {
				$return = array(
					'state'	=>	'error'
				);
				return json_encode($return);
			}
		}
	}
?>