<?php
	class Routing {
		public function get($routing) {
			if (!empty($routing)) {
				$routing = str_replace("..", "", explode("/", $routing));
				if ($routing[0] == 'ru' || $routing[0] == 'en') {
					$i18n_check = $routing[0];
					$set_lang = '/'.$routing[0];
					array_splice($routing, 0, 1);
				}
				if (empty($routing[0])) {
					if (!isset($i18n_check)) {
						$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
						switch ($lang){
							case "ru":
								$i18n_check = 'ru';
							break;
							case "en":
								$i18n_check = 'en';
							break;
							default:
								$i18n_check = 'en';
							break;
						}
					}
					$route = @array(
						'controller'	=>	"$_SERVER[DOCUMENT_ROOT]/controllers/index.php",
						'view'			=>	"app/index.twig",
						'i18n'			=>	$i18n_check,
						'set_lang'		=>	$set_lang
					);
					return $route;
				}
				if (file_exists("$_SERVER[DOCUMENT_ROOT]/controllers/$routing[0].php") && file_exists("$_SERVER[DOCUMENT_ROOT]/template/app/$routing[0].twig")) {
					if (!isset($i18n_check)) {
						$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
						switch ($lang){
							case "ru":
								$i18n_check = 'ru';
							break;
							case "en":
								$i18n_check = 'en';
							break;
							default:
								$i18n_check = 'en';
						}
					}
					$route = @array(
						'controller'	=>	"$_SERVER[DOCUMENT_ROOT]/controllers/$routing[0].php",
						'view'			=>	"app/$routing[0].twig",
						'routing'		=>	$routing,
						'i18n'			=>	$i18n_check,
						'set_lang'		=>	$set_lang
					);
					return $route;
				}
				elseif ($routing[0] == 'receiver' && !empty($routing[1])) {
					if (!isset($i18n_check)) {
						$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
						switch ($lang){
							case "ru":
								$i18n_check = 'ru';
							break;
							case "en":
								$i18n_check = 'en';
							break;
							default:
								$i18n_check = 'en';
						}
					}
					$route = @array(
						'controller'	=>	"$_SERVER[DOCUMENT_ROOT]/controllers/receiver/$routing[1].php",
						'i18n'			=>	$i18n_check,
						'set_lang'		=>	$set_lang
					);
					return $route;
				}
				else {
					$route = @array(
						'controller'	=>	"$_SERVER[DOCUMENT_ROOT]/controllers/404.php",
						'i18n'			=>	$i18n_check,
						'set_lang'		=>	$set_lang,
						'routing'		=>	$routing,
					);
					return $route;
				}
			}
			else {
				$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
				switch ($lang){
					case "ru":
						$i18n_check = 'ru';
					break;
					case "en":
						$i18n_check = 'en';
					break;
					default:
						$i18n_check = 'en';
					break;
				}
				$route = @array(
					'controller'	=>	"$_SERVER[DOCUMENT_ROOT]/controllers/index.php",
					'view'			=>	"app/index.twig",
					'i18n'			=>	$i18n_check,
					'set_lang'		=>	$set_lang
				);
				return $route;
			}
		}
	}
?>