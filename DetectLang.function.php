<?php
function DetectLang($encoded_query) {
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211);
	if (!empty($encoded_query['callback_query'])) {
		$encoded_query['message'] = $encoded_query['callback_query']['message'];
		$encoded_query['message']['from'] = $encoded_query['callback_query']['from'];
	}
	if (!empty($encoded_query['message']['from']['id']) && !empty($encoded_query['message']['chat']['id'])) {
		$telegram_id = $encoded_query['message']['from']['id'];
		$lang_cache = @$memcache->get("lang_$telegram_id");
		if (empty($lang_cache)) {
			$check_lang = mysqli_query($mysqli, "SELECT `lang` FROM `users` WHERE `telegram_id`='$telegram_id';");
			$check_lang = mysqli_fetch_row($check_lang)[0];
			if (!empty($check_lang)) {
				$lang = $check_lang;
				$memcache->set("lang_$telegram_id", $lang, MEMCACHE_COMPRESSED, 600);
			}
		}
		else {
			$lang = $lang_cache;
		}
		if (empty($lang)) {
			$chat = $encoded_query['message']['chat']['id'];
			$lang_cache = @$memcache->get("lang_$chat");
			if (empty($lang_cache)) {
				$check_lang = mysqli_query($mysqli, "SELECT `lang` FROM `chats` WHERE `chat`='$chat';");
				$check_lang = mysqli_fetch_row($check_lang)[0];
				if (!empty($check_lang)) {
					$lang = $check_lang;
					$memcache->set("lang_$chat", $lang, MEMCACHE_COMPRESSED, 600);
				}
				else {
					$lang = 'en';
				}
			}
			else {
				$lang = $lang_cache;
			}
		}
		require $_SERVER['DOCUMENT_ROOT'].'/i18n/'.$lang.'.php';
		return $i18n;
	}
}
?>